
package com.restpro.modulo1.entities;


import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.log4j.Logger;

/**
 * En este apartado la entidad define la tabla "cliente" y así mismo
 * los diferentes campos que la conforman.  
 * @author Miguel Cruz
 * @version 1.0
 * @since Modulo1
 */
@Entity
@Table(name = "cliente", catalog = "usuario", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findById", query = "SELECT c FROM Cliente c WHERE c.id = :id")
    , @NamedQuery(name = "Cliente.findByNombreCliente", query = "SELECT c FROM Cliente c WHERE c.nombreCliente = :nombreCliente")
    , @NamedQuery(name = "Cliente.findByApellidoPaterno", query = "SELECT c FROM Cliente c WHERE c.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Cliente.findByApellidoMaterno", query = "SELECT c FROM Cliente c WHERE c.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Cliente.findByNombreEmpresa", query = "SELECT c FROM Cliente c WHERE c.nombreEmpresa = :nombreEmpresa")
    , @NamedQuery(name = "Cliente.findByCargo", query = "SELECT c FROM Cliente c WHERE c.cargo = :cargo")})


/**
 * Esta clase se utiliza para la creación de un registro cliente
 * @author Miguel Cruz
 * @version 1.0
 * @since Modulo1
 */

public class Cliente implements Serializable {
//Creación del archivo.log
    private static Logger log = Logger.getLogger(Cliente.class);
    
    public static void main(String[] args)
{
    if (log.isTraceEnabled())
    {
        log.trace("mensaje de trace");
    }

    if (log.isDebugEnabled())
    {
        log.debug("mensaje de debug");
    }

    if (log.isInfoEnabled())
    {
        log.info("mensaje de info");
    }
    
    log.warn("mensaje de warn");
    log.error("mensaje de error");
    log.fatal("mensaje de fatal");
}
    //Define el tamaño de los campos y si son o no son Nulos
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_cliente", nullable = false, length = 100)
    private String nombreCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellido_paterno", nullable = false, length = 100)
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "apellido_materno", nullable = false, length = 100)
    private String apellidoMaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_empresa", nullable = false, length = 100)
    private String nombreEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cargo", nullable = false, length = 100)
    private String cargo;
    
    //Crea una nueva instancia de Cliente
    public Cliente() {
    }
    
    public Cliente(Integer id) {
        this.id = id;
    }
    
    /**
     * Constructor para crear un cliente
     * @param id entero = id
     * @param nombreCliente cadena de caracteres = nombreCliente
     * @param apellidoPaterno cadena de caracteres = apellidoPaterno
     * @param apellidoMaterno cadena de caracteres = apellidoMaterno
     * @param nombreEmpresa cadena de caracteres = nombreEmpresa
     * @param cargo cadena de caracteres = cargo
     */
    
    public Cliente(Integer id, String nombreCliente, String apellidoPaterno, String apellidoMaterno, String nombreEmpresa, String cargo) {
        this.id = id;
        this.nombreCliente = nombreCliente;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.nombreEmpresa = nombreEmpresa;
        this.cargo = cargo;
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="Metodos GETTER">
    
    /**
     * Método para obtener la Id del cliente 
     * @return la id como valor entero
     */
    
    public Integer getId() {
        return id;
    }

    /**
     * Método para obtener el nombreCliente
     * @return el nombreCliente como una cadena de caracteres
     */
    
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * Método para obtener el apellidoPaterno del cliente
     * @return el apellidoPaterno como una cadena de caracteres
     */
    
    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    /**
     * Método para obtener el apellidoMaterno del cliente
     * @return el apellidoMaterno como una cadena de caracteres
     */
    
    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    /**
     * Método para obtener el nombreEmpresa del cliente
     * @return el nombreEmpresa como una cadena de caracteres
     */
    
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * Método para obtener el cargo del cliente
     * @return el cargo como una cadena de caracteres
     */
    
    public String getCargo() {
        return cargo;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos SETTER">
    
    /**
 * Método para asignar un parametro a Id
 * @param id establece el tipo de parametro que recibe
 */
    
    public void setId(Integer id) {
        this.id = id;
    }

    /** 
     * Método para asignar un parametro a nombreCliente
     * @param nombreCliente establece el tipo de parametro que recibe
     */
    
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * Método para asignar un parametro a apellidoPaterno
     * @param apellidoPaterno establece el tipo de parametro que recibe 
     */
    
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    
    /**
     * Método para asignar un parametro a apellidoMaterno
     * @param apellidoMaterno establece  el tipo de parametro que recibe
     */
    
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    /**
     * Método para asignar un parametro a nombreEmpresa
     * @param nombreEmpresa establece el tipo de parametro que recibe
     */
    
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /** 
     * Método para asignar un parametro a cargo
     * @param cargo establece el tipo de parametro que recibe
     */
    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos HASHCODE y EQUALS">
    @Override
   
   /**Método para comparar y almacenar los campos en su espacio 
    correspondiente
    */
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.nombreCliente);
        hash = 17 * hash + Objects.hashCode(this.apellidoPaterno);
        hash = 17 * hash + Objects.hashCode(this.apellidoMaterno);
        hash = 17 * hash + Objects.hashCode(this.nombreEmpresa);
        hash = 17 * hash + Objects.hashCode(this.cargo);
        return hash;
    }

    @Override
    
    /**
     * Método para comparar referencias entre objetos
     */
    
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.nombreCliente, other.nombreCliente)) {
            return false;
        }
        if (!Objects.equals(this.apellidoPaterno, other.apellidoPaterno)) {
            return false;
        }
        if (!Objects.equals(this.apellidoMaterno, other.apellidoMaterno)) {
            return false;
        }
        if (!Objects.equals(this.nombreEmpresa, other.nombreEmpresa)) {
            return false;
        }
        if (!Objects.equals(this.cargo, other.cargo)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
//</editor-fold>    
}


