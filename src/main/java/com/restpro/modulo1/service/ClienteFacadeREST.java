/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo1.service;

import com.restpro.modulo1.dao.UsuarioA;
import com.restpro.modulo1.entities.Cliente;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Esta clase es utilizada para consumir y producir peticiones HTTP en formato
 * JSON
 *
 * @author Miguel Cruz
 * @version 1.0
 * @since Modulo1
 * @see AbstractFacade
 */
@Stateless
@Path("cliente")
public class ClienteFacadeREST extends AbstractFacade<Cliente> {

    //@PersistenceContext(unitName = "com.RestPro_Modulo1_war_1.0PU")
    private EntityManager em;
    private String JSON_RESPONSE = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";

    /**
     * Llamar al constructor de la clase padre (Cliente)
     */
    public ClienteFacadeREST() {
        super(Cliente.class);
    }

    /**
     * Método para consumir y producir en formato JSON la insercción de
     * información de manera adecuada
     *
     * @param entity obtiene la id del registro solicitado
     * @return una respuesta en formato JSON de la petición utilizada
     */
    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String create(Cliente entity) {
        String resultado = super.create(entity);
        return String.format(JSON_RESPONSE, entity.getId(), "INSERT", resultado);
    }

    /**
     * Método para consumir y producir en formato JSON la actualización de
     * información de manera correcta
     *
     * @param id recibe la id del registro para poder actualizar
     * @param entity obtiene el registro a actualizar
     * @return una respuesta en formato JSON de la petición utilizada
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validar(Cliente cliente) {
        boolean status = UsuarioA.validar(cliente.getNombreCliente(), cliente.getApellidoPaterno(), cliente.getApellidoMaterno(), cliente.getNombreEmpresa(), cliente.getCargo());
        if (status) {
            String HASH = "AHGC-12BD-1328-75HF-HD64";

            JsonObject json = Json.createObjectBuilder()
                    .add("token", HASH)
                    .build();
            return Response.status(Response.Status.CREATED).entity(json).build();
        }
        JsonObject json = Json.createObjectBuilder()
                .add("mensaje", "Datos incorrectos")
                .build();
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(json)
                .build(); 
    }
                    
                    
                    
    @PUT            
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)

    public String edit(@PathParam("id") Integer id, Cliente entity) {
        String resultado = super.edit(entity);
        return String.format(JSON_RESPONSE, entity.getId(), "UPDATE", resultado);
    }

    /**
     * Método para eliminar por medio de la id un registro
     *
     * @param id recibe el paramatro que se desea eliminar
     * @return una respuesta en formato JSON de la petición utilizada
     */
    @DELETE
    @Path("{id}")
    public String remove(@PathParam("id") Integer id) {
        String resultado = super.remove(super.find(id));
        String mensaje = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";
        return String.format(mensaje, id, "DELETE", resultado);
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Cliente find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Cliente> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Cliente> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        if (em == null) {
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("com.RestPro_Modulo1_war_1.0PU");
            em = factory.createEntityManager();
        }
        return em;
    }

}
