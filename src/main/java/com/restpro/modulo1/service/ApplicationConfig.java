/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo1.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *Clase de configruación de aplicación 
 * @author Miguel Cruz
 * @version 1.0
 * @since Modulo1
 * @see Application
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.restpro.modulo1.service.ClienteFacadeREST.class);
        resources.add(com.restpro.modulo1.service.Seguridad.class);
    }
    
}
