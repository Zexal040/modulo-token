/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo1.service;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *Implementación común para ayudantes basados en el tipo de enlace
 * @author Miguel Cuz
 * @version 1.0
 * @since Modulo1
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    /**
     * Método para indicar que se creó de manera adecuada un registro
     * @param entity hace una instancia persistente y actualiza el estado de la
     * base de datos
     * @return un mensaje satisfactorio sí es que la transacción fue exitosa
     */
    
    public String create(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
            em.refresh(entity);
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
 
    }

    /**
     * Método para indicar que se actualizó de manera adecuada un registro
     * @param entity hace una instancia persistente y actualiza el estado de la
     * base de datos
     * @return un mensaje satisfactorio sí es que la transacción fue exitosa
     */
    
    public String edit(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
        
    }

    /**
     * Método para indicar que se eliminó de manera adecuada un registro
     * @param entity hace una instancia persistente y actualiza el estado de la
     * base de datos
     * @return un mensaje satisfactorio sí es que la transacción fue exitosa
     */
    
    public String remove(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.remove(em.merge(entity));
            em.getTransaction().commit();
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
    }

    /**
     * Método para buscar registros por medio de la id
     * @param id establece el tipo de parametro que recibe
     * @return el registro correspondiente a la id elegida
     */
    
    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    /**
     * Método para obtener una lista de todos los campos del registro
     * @return la lista del registro deseado
     */
    
    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    
    /**
     * Método para buscar por su rango el registro deseado
     * @param range establece el tipo de parametro que recibe
     * @return la lista del registro deseado
     */
    
    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    /**
     * Método para obtener el número de registros 
     * @return la cantidad de registros 
     */
    
    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
